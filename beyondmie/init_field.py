import math
import numpy as np

def plane_wave_coefficients(epol,kth,kphi,m,n):
    """Note that epol has to be in spherical coordinates and we assume that the particle
    is in the center of coordinate system"""
    Y,mvec,nvec=math.vector_spherical_harmonics(n,m,kth,kphi)
    norm_conv=2j/np.sqrt(4*np.pi) # if they are supposed to be smuthi-compatible
    #norm_conv=1
    return 4*1j**n*np.dot(epol,mvec.conj())/norm_conv,-4*1j**(n+1)*np.dot(epol,nvec.conj())/norm_conv

def intial_field_vector(kth=0,kphi=0,nmax=1,epol_cart=[0,1,0]):
    vvec,pvec=generate_grid(nmax)
    a=np.zeros(vvec.shape,dtype=complex)
    b=np.zeros(vvec.shape,dtype=complex)
    for ip,p in enumerate(vvec):
        n,m = p_to_nm(p)
        epol=math.transformation_matrix(kth,kphi)@epol_cart
        Y,mvec,nvec=math.vector_spherical_harmonics(n,m,kth,kphi)    
        a[ip],b[ip]=plane_wave_coefficients(epol,kth,kphi,m,n)
    return np.concatenate((a,b))
