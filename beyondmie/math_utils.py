import scipy.special
from scipy.special import riccati_yn,riccati_jn,lpmn
from math import factorial
import numpy as np

sbessel = scipy.special.spherical_jn
def shankel(n, x):
    spherj = scipy.special.spherical_jn(n, x)
    sphery = scipy.special.spherical_yn(n, x)
    if hasattr(x, '__len__'):
        sphery[x==0] = np.nan
    return spherj + 1j * sphery

def psi(n,x):
    return sbessel(n, x)*x

def ksi(n,x):
    return shankel(n, x)*x

def psi_p(n, x):
    res = x * sbessel(n - 1, x) - n * sbessel(n, x)
    return res

def ksi_p(n, x):
    res = x * shankel(n - 1, x) - n * shankel(n, x)
    return res

def transformation_matrix(theta,phi):
    R=np.array([[np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)],
                [np.cos(theta)*np.cos(phi),np.cos(theta)*np.sin(phi),-np.sin(theta)],
                [-np.sin(phi),np.cos(phi),0]])
    return R

def transform_matrix(epsilon,theta,phi):
    R=transformation_matrix(theta,phi)
    return R@epsilon@R.T

def vector_spherical_harmonics(n,m,theta,phi):
    theta=theta+1e-6
    Pmn,Pmn_prim=lpmn(m,n,np.cos(theta))
    Pmn=Pmn[-1,-1]
    Pmn_prim=Pmn_prim[-1,-1]
    Pmn_prim=Pmn_prim*(-np.sin(theta)) # cosine derivative is minus sine
    norm_l=np.sqrt((2*n+1)/(4*np.pi)*factorial(n-m)/factorial(n+m))
    expim=np.exp(1j*m*phi)
    Y=norm_l*Pmn*np.array([1,0,0])
    u=1/np.sqrt(n*(n+1))*m/np.sin(theta)*norm_l*Pmn
    s=1/np.sqrt(n*(n+1))*norm_l*Pmn_prim
    X=1j*u*expim*np.array([0,1,0])-s*expim*np.array([0,0,1])
    Z=s*expim*np.array([0,1,0])+1j*u*expim*np.array([0,0,1]) # could be refactored
    if n==0 and m==0:
        X=0*np.array([0,0,1])
        Z=0*np.array([0,0,1]) 
    return Y,X,Z

