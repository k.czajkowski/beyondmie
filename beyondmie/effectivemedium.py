mport numpy as np
import scipy.interpolate as sinterp

def fillfactor(wd,wm):
    return wm/(wd+wm)

def eps_parallel(eps_m,eps_d,f):   
    return eps_m*eps_d/((1-f)*eps_m+f*eps_d)
           
def eps_transverse(eps_m,eps_d,f):
    return (1-f)*eps_d+f*eps_m    
              
def effective_permittivity(eps_m,eps_d,wm,wd):
    """ based on: https://doi.org/10.1364/OE.21.019113 """
    f=fillfactor(wd,wm)
    eps_p = eps_parallel(eps_m,eps_d,f)
    eps_t = eps_transverse(eps_m,eps_d,f)
    return np.diag(np.array([eps_t,eps_t,eps_p]))

def set_drude(wl, ep, g, e0):
    """
    Creates a vector y with Drude permittivity
    Input:
    wl - wavelength vector in nm
    ep - resonance energy in eV
    g - gamma in eV
    e0 - eps_inf
    """
    w = 1239.84 / (wl * 10 ** 9)
    y = e0 - ep ** 2 / (w ** 2 + 1j * w * g)
    return y

def LoadNkData(wl, fname, delim=';', units=1000, omit=1):
    """ Loads nk data from file """
    rawd = np.loadtxt(fname, delimiter=delim, skiprows=omit)
    wl0 = rawd[:, 0] * units
    nd = rawd[:, 1]
    kd = rawd[:, 2]
    ni = sinterp.CubicSpline(wl0, nd)
    ki = sinterp.CubicSpline(wl0, kd)
    return ni(wl) + 1j * ki(wl)

def LoadNkDataEff(wl, fname, delim=' ', units=1, omit=1):
    """ Loads nk data from file """ 
    rawd = np.loadtxt(fname, delimiter=delim, skiprows=omit)
    wl0 = rawd[:, 0] * units
    nd = rawd[:, 1]
    kd = rawd[:, 2]
    ndz = rawd[:, 5]
    kdz = rawd[:, 6]
    ni = sinterp.CubicSpline(wl0, nd)
    ki = sinterp.CubicSpline(wl0, kd)
    niz = sinterp.CubicSpline(wl0, ndz)
    kiz = sinterp.CubicSpline(wl0, kdz)
    return np.sqrt(ni(wl) + 1j * ki(wl)), np.sqrt(niz(wl) + 1j * kiz(wl))

