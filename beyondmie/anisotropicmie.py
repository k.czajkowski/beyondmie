import scipy.special
from scipy.special import riccati_yn,riccati_jn,lpmn
from math import factorial
import numpy as np
import beyondmie.math_utils as math

sbessel = scipy.special.spherical_jn
def shankel(n, x):
    spherj = scipy.special.spherical_jn(n, x)
    sphery = scipy.special.spherical_yn(n, x)
    if hasattr(x, '__len__'):
        sphery[x==0] = np.nan
    return spherj + 1j * sphery

def psi(n,x):
    return sbessel(n, x)*x

def ksi(n,x):
    return shankel(n, x)*x

def psi_p(n, x):
    res = x * sbessel(n - 1, x) - n * sbessel(n, x)
    return res

def ksi_p(n, x):
    res = x * shankel(n - 1, x) - n * shankel(n, x)
    return res



def generate_grid(nmax):
    pmax=nmax**2+2*nmax # p is single (n,m) coefficient
    v=np.arange(1,pmax+1)
    p=np.arange(1,pmax+1)
    return v,p

def angle_from_multi(nth,nphi,nmax):
    theta=np.pi*nth/(2*nmax)
    if nth<=nmax:
        phi=np.pi*(2*nphi+1)/(nth+1)
    else:
        phi=np.pi*(2*nphi+1)/(2*nmax-nth+1)
    return theta,phi

def single_coefficient(nth,nphi,nmax):
    if nth<=nmax:
        v=nth*(nth+1)/2+nphi+1
    else:
        v=(nmax+1)**2-(2*nmax-nth)*(2*nmax-nth+3)/2+nphi
    return v

def multi_coefficients(v,nmax):
    if v<=(nmax+1)*(nmax+2)/2:
        nth=int(np.sqrt(2*v-1)-1/2)
        nphi=v-nth*(nth+1)/2-1
    else:
        nth=2*nmax-int(np.sqrt(2*(nmax+1)**2-2*v-1)-1/2)
        nphi=v+(2*nmax-nth)*(2*nmax-nth+3)/2-(nmax+1)**2        
    return nth,nphi

def p_to_nm(p):
    n=int(np.sqrt(p))
    m=p-n*(n+1)
    return n,m

def transformation_matrix(theta,phi):
    R=np.array([[np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)],
                [np.cos(theta)*np.cos(phi),np.cos(theta)*np.sin(phi),-np.sin(theta)],
                [-np.sin(phi),np.cos(phi),0]])
    return R

def transform_matrix(epsilon,theta,phi):
    R=transformation_matrix(theta,phi)
    return R@epsilon@R.T

def eigenvalues(epsilon,theta,phi):
    er_r=epsilon[0,0]
    eph_ph=epsilon[2,2]
    eth_th=epsilon[1,1]
    er_ph=epsilon[0,2]
    er_th=epsilon[0,1]
    eph_r=epsilon[2,0]
    eth_r=epsilon[1,0]
    gam=np.linalg.det(epsilon)
    alf=er_r
    beta=alf*(eth_th+eph_ph)-er_th*eth_r-er_ph*eph_r
    delta=beta**2-4*alf*gam+0j
    k2falkaprim=(beta+np.sqrt(delta))/(2*alf)
    k2falkabis=(beta-np.sqrt(delta))/(2*alf)
    k1falka=np.sqrt(k2falkaprim)
    k2falka=np.sqrt(k2falkabis)
    #assert np.abs(np.linalg.det(np.array([[epsilon[0,2],epsilon[0,1]],[epsilon[1,2],epsilon[1,1]-k2falka**2]])))<1e-10
    return k1falka,k2falka

def eigenvectors(epsilon):
    # I implement only case 3, cause we don't care about others
    er_r=epsilon[0,0]
    er_th=epsilon[0,1]    
    gam1=np.array([0,0,1])
    gam2=-er_th/er_r*np.array([1,0,0])+np.array([0,1,0])
    return gam1,gam2

def vector_spherical_harmonics(n,m,theta,phi):
    theta=theta+1e-6
    Pmn,Pmn_prim=lpmn(m,n,np.cos(theta))
    Pmn=Pmn[-1,-1]
    Pmn_prim=Pmn_prim[-1,-1]
    Pmn_prim=Pmn_prim*(-np.sin(theta)) # cosine derivative is minus sine
    norm_l=np.sqrt((2*n+1)/(4*np.pi)*factorial(n-m)/factorial(n+m))
    expim=np.exp(1j*m*phi)
    Y=norm_l*Pmn*np.array([1,0,0])
    u=1/np.sqrt(n*(n+1))*m/np.sin(theta)*norm_l*Pmn
    s=1/np.sqrt(n*(n+1))*norm_l*Pmn_prim
    X=1j*u*expim*np.array([0,1,0])-s*expim*np.array([0,0,1])
    Z=s*expim*np.array([0,1,0])+1j*u*expim*np.array([0,0,1]) # could be refactored
    if n==0 and m==0:
        X=0*np.array([0,0,1])
        Z=0*np.array([0,0,1]) 
    return Y,X,Z

def alpha_coeffs(n,X,Y,Z,gam):
    alf=np.sqrt(n*(n+1))
    alf_h=4*np.pi*1j**n*X.conj()@gam
    alf_e=4*np.pi*1j**n*Z.conj()@gam/1j
    alf_o=4*np.pi*1j**n*Y.conj()@gam/1j
    return alf,alf_e,alf_h,alf_o

def U_matrix_piece(alf,alf_e,alf_h,alf_o,ke,kv,R,n):
    Uh=alf_h*(ke/kv*psi(n,kv*R)*psi_p(n,ke*R)-psi_p(n,kv*R)*psi(n,ke*R))
    Ue=alf_e*(psi(n,kv*R)*psi_p(n,ke*R)-ke/kv*psi_p(n,kv*R)*psi(n,ke*R))-(ke/kv)**2*alf*alf_o*psi(n,kv*R)*psi(n,ke*R)/(ke*R)
    return Uh,Ue

def V_matrix_piece(alf,alf_e,alf_h,alf_o,ke,kv,R,n):
    Vh=alf_h*(psi_p(n,kv*R)*ksi(n,ke*R)-ke/kv*psi(n,kv*R)*ksi_p(n,ke*R))
    Ve=alf_e*(ke/kv*psi_p(n,kv*R)*ksi(n,ke*R)-psi(n,kv*R)*ksi_p(n,ke*R))+alf*alf_o*(ke/kv)**2*psi(n,kv*R)*ksi(n,ke*R)/(ke*R)
    return Vh,Ve

def solve_T_matrix(U,V):
    T=U@np.linalg.inv(V)
    return T

def compute_T_matrix(epsilon_cart,k0,nsurr,R,nmax):
    ke=k0*nsurr
    vvec,pvec=generate_grid(nmax)
    pmax=len(pvec)
    Uh=np.zeros((len(pvec),len(pvec),2),dtype=complex)
    Ue=np.zeros((len(pvec),len(pvec),2),dtype=complex)
    Vh=np.zeros((len(pvec),len(pvec),2),dtype=complex)
    Ve=np.zeros((len(pvec),len(pvec),2),dtype=complex)
    U=np.zeros((2*len(pvec),2*len(pvec)),dtype=complex)
    V=np.zeros((2*len(pvec),2*len(pvec)),dtype=complex)
    for ip,p in enumerate(pvec):
        n,m = p_to_nm(p)
        for iv,v in enumerate(vvec):
            nth,nphi=multi_coefficients(v,nmax)
            theta,phi=angle_from_multi(nth,nphi,nmax)
            Y,X,Z = vector_spherical_harmonics(n,m,theta,phi)
            epsilon=transform_matrix(epsilon_cart,theta,phi) # tu nie jestem pewien za bardzo
            #epsilon=transform_matrix(epsilon_cart,0,0)
            k1falka,k2falka=eigenvalues(epsilon,theta,phi)
            gam1,gam2=eigenvectors(epsilon)
            gam=gam1+0
            kv=k1falka*k0
            for iev in range(2):
                alf,alf_e,alf_h,alf_o=alpha_coeffs(n,X,Y,Z,gam)
                Uh[ip,iv,iev],Ue[ip,iv,iev]=U_matrix_piece(alf,alf_e,alf_h,alf_o,ke,kv,R,n)
                Vh[ip,iv,iev],Ve[ip,iv,iev]=V_matrix_piece(alf,alf_e,alf_h,alf_o,ke,kv,R,n)
                gam=gam2+0
                kv=k2falka*k0
    # wype³niam macierze U i V po cwiartce
    U[:pmax,:pmax]=Uh[:,:,0]
    U[:pmax,pmax:]=Uh[:,:,1]
    U[pmax:,:pmax]=Ue[:,:,0]    
    U[pmax:,pmax:]=Ue[:,:,1]
    V[:pmax,:pmax]=Vh[:,:,0]
    V[:pmax,pmax:]=Vh[:,:,1]
    V[pmax:,:pmax]=Ve[:,:,0]
    V[pmax:,pmax:]=Ve[:,:,1]
    return solve_T_matrix(U,V)

def plane_wave_coefficients(epol,kth,kphi,m,n):
    """Note that epol has to be in spherical coordinates and we assume that the particle
    is in the center of coordinate system"""
    Y,mvec,nvec=math.vector_spherical_harmonics(n,m,kth,kphi)
    norm_conv=2j/np.sqrt(4*np.pi) # if they are supposed to be smuthi-compatible
    #norm_conv=1
    return 4*1j**n*np.dot(epol,mvec.conj())/norm_conv,-4*1j**(n+1)*np.dot(epol,nvec.conj())/norm_conv

def intial_field_vector(kth=0,kphi=0,nmax=1,epol_cart=[0,1,0]):
    vvec,pvec=generate_grid(nmax)
    a=np.zeros(vvec.shape,dtype=complex)
    b=np.zeros(vvec.shape,dtype=complex)
    for ip,p in enumerate(vvec):
        n,m = p_to_nm(p)
        epol=math.transformation_matrix(kth,kphi)@epol_cart
        Y,mvec,nvec=math.vector_spherical_harmonics(n,m,kth,kphi)    
        a[ip],b[ip]=plane_wave_coefficients(epol,kth,kphi,m,n)
    return np.concatenate((a,b))

def calculate_cross_sections(T,kth,kphi,nmax,epol_cart,ks):
    # ks - k in the medium
    init=intial_field_vector(kth,kphi,nmax,epol_cart)
    scat=T@init
    cext=-np.pi/ks**2*np.real(init.conj()@scat)
    cscat=np.pi/ks**2*np.sum(np.abs(scat)**2) 
    return cext,cscat 

