import scipy.io as sio
import beyondmie.anisotropicmie as amie
import matplotlib.pyplot as plt
import numpy as np

n0=4+1j
ref_ind=np.array([0]*3)+n0
epsilon_cart=np.diag(ref_ind**2)
R=75
nsurr=1
nmax=1
kth=0
kphi=0
nmax=3
epol_cart=[0,1,0]
wlvec=np.linspace(400,1000,100)
#wlvec=np.array([500])
fname = 'result_iso.mat'

s=np.pi*R**2

cext=np.zeros(wlvec.shape)
cscat=np.zeros(wlvec.shape)
for iwl,wl in enumerate(wlvec):
    k0=2*np.pi/wl
    ks=k0*nsurr
    T=amie.compute_T_matrix(epsilon_cart,k0,nsurr,R,nmax)
    cext[iwl],cscat[iwl]=amie.calculate_cross_sections(T,kth,kphi,nmax,epol_cart,ks) 
    print((wl,cext[iwl]/s,cscat[iwl]/s))  

plt.figure()
plt.plot(wlvec,cext/s)
plt.plot(wlvec,cscat/s)
plt.show()
#print((3.58,1.798))

wyn={'cext':cext,'cscat':cscat,'wlvec':wlvec}    
sio.savemat(fname,wyn)

