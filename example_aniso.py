import scipy.io as sio
import anisotropicmie as amie
import numpy as np

ref_ind=np.array([2,2,4])
epsilon_cart=np.diag(ref_ind**2)
R=75
nsurr=1
nmax=1
kth=0
kphi=0
nmax=5
epol_cart=[1,0,0]
wlvec=np.linspace(400,1000,100)
fname = 'result_aniso.mat'

cextaniso=np.zeros(wlvec.shape)
cscataniso=np.zeros(wlvec.shape)
for iwl,wl in enumerate(wlvec):
    print(wl)
    k0=2*np.pi/wl
    ks=k0*nsurr
    T=amie.compute_T_matrix(epsilon_cart,k0,nsurr,R,nmax)
    cextaniso[iwl],cscataniso[iwl]=amie.calculate_cross_sections(T,kth,kphi,nmax,epol_cart,ks)   

wyn={'cextaniso':cextaniso,'cscataniso':cscataniso,'wlvec':wlvec}    
sio.savemat(fname,wyn)
