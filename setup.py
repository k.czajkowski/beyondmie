from distutils.core import setup

setup(
    name='beyondmie',
    version='0.1dev',
    packages=['beyondmie'],
    install_requires=['scipy']
)

